const {app, BrowserWindow} = require('electron');

let mainWindow;

function createMainWindow() {
    mainWindow = new BrowserWindow({width: 800, height: 600});

    mainWindow.loadURL('file:://' + __dirname + '/index.html');

    mainWindow.on('closed', function() {
	mainWindow = null;
    });
}

app.on('ready', createMainWindow);

// Quit when all windows are closed
// except on MacOS
app.on('window-all-closed', function() {
    if (process.platform !== 'darwin')
	app.quit();
});
